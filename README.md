<h1>Evaluación Talana</h1>

<h2>El Desarrollo</h2>
En este breve repo podrán validar la solución propuesta, la cual consta de un archivo ci que automatiza el proceso de despliegue, cargando las imagenes en un primer stage "build" y luego les hace pull para un proceso "deploy", en el cuál se aplican un par de manifiestos k8s para el autoescalado y despliegue del mismo, en estos se definieron como valores para levantar pods adicionales un 50% de carga de CPU, a la vez que se define una cantidad mínima y máxima de pods de 1 y 10, respectivamente, además de definir la política de reinicio en siempre de modo que se asegure la disponibilidad del pod en cuestión. Los jobs dentro del ci se ejecutan ante cambios en la rama y ejecución manual, mientras que por el deploy, este job sólo se ejecutará previa autorización manual, ya que preferí simulara un despliegue lo más real posible.

<h2>Dificultades observadas</h2>
En la realización encontré varios obstaculos, como el hacer el despliegue desde el compose, ya que usualmente sólo he trabajado con soluciones donde cada servicio maneja un despliegue individual, muy orientado a microservicios, tomó un par de vueltas más dar con como desplegar esto. Por lo demás, la imagen que uso no contiene kubectl, y no fui capaz dado el tiempo de construirla personalmente, mis disculpas por ello. Adicionalmente, creo haber dado con todo lo solicitado.
